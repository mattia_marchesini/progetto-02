#ifndef __TEMPERATURESENSOR__
#define __TEMPERATURESENSOR__

#define ANALOG_TO_CELSIUS_COEFF 0.48828125

class TemperatureSensor {

  public:
    TemperatureSensor(int pin);
    float getTemperature();

  private:
    int pin;
};

#endif
