#include "SonarImpl.h"
#include <arduino.h>

SonarImpl::SonarImpl(int echoPin, int trigPin, int vccPin, float temperature): Sonar(echoPin, trigPin, temperature) {
  this->init(vccPin);
}

SonarImpl::SonarImpl(int echoPin, int trigPin, int vccPin): Sonar(echoPin, trigPin, STANDARD_TEMP) {
  this->init(vccPin);
}

void SonarImpl::init(int vccPin) {
  this->vccPin = vccPin;
  pinMode(vccPin, OUTPUT);
  digitalWrite(vccPin, HIGH);
}
