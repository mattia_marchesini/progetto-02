#include "ServoMotorImpl.h"
#include <arduino.h>

ServoMotorImpl::ServoMotorImpl(int pin){
  this->pin = pin;  
} 

void ServoMotorImpl::on(){
  motor.attach(pin);  
}

void ServoMotorImpl::setPosition(int angle){
  if (angle > 180){
    angle = 180;
  } else if (angle < 0){
    angle = 0;
  }

  // min_pulse -> 0°, max_pulse -> 180°
  // min and max values could change: see ServoTimer2.h for more information
  // you need to write min_pulse + angle * (max_pulse - min_pulse) / 180
  float coeff = ((float) MAX_PULSE_WIDTH - MIN_PULSE_WIDTH)/180;
  motor.write(MIN_PULSE_WIDTH + angle*coeff);              
}

void ServoMotorImpl::off(){
  motor.detach();    
}
