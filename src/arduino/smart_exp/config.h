#ifndef __CONFIG__
#define __CONFIG__

/*RADAR*/
#define RADAR_TRIG_PIN 7
#define RADAR_ECHO_PIN 6
#define RADAR_VCC_PIN 8
//#define SONAR_MAX_DIST 1

/*SERVO*/
#define SERVO_IN_PIN A0

/*LED*/
#define RED_LED_PIN 13
#define GREEN_LED_PIN 12 

/*BUTTON*/
#define BUTTON_START_PIN 
#define BUTTON_STOP_PIN 

/*PIR*/
#define PIR_OUT_PIN 4
#define PIR_CALIBRATION_TIME 10000 //milliseconds

/*POTENTIOMETER*/
#define POT_OUT_PIN A0
#define POT_VCC_PIN A5
#define POT_MAX_READ 1023

/*SONAR*/
//#define 

/*OTHER*/
#define SLEEP_TIME 5000 //milliseconds
#define MAX_TIME 20000
#define ERROR_TIME 2000
#define MIN_FREQ 1 // Hz
#define MAX_FREQ 50
#define MAX_VEL TBD 

/*DEBUG*/
#define DEBUG
#ifdef DEBUG
#define DEBUG_PRINT(...) do{ Serial.println( __VA_ARGS__ ); Serial.flush();} while( false )
#else
#define DEBUG_PRINT(...) do{ } while ( false )
#endif

#endif
