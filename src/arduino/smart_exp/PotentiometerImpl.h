#ifndef __POTENTIOMETERIMPL__
#define __POTENTIOMETERIMPL__

#include "Potentiometer.h"

class PotentiometerImpl: public Potentiometer {
  public:
    PotentiometerImpl(int outPin, int vccPin, int maxRead, int minMappedValue, int maxMappedValue);
    int getValue();

  private:
    int vccPin, minMappedValue, maxMappedValue;
};

#endif
