#include "Pir.h"
#include <arduino.h>
#include "config.h"

Pir::Pir(int pin) {
  this->pin = pin;
  pinMode(pin, INPUT);
}

bool Pir::isDetected() {
  return digitalRead(pin) == HIGH;
}

void Pir::calibrate() {
  DEBUG_PRINT("calibrating pir");
  delay(PIR_CALIBRATION_TIME);
  DEBUG_PRINT("calibrated");
}
