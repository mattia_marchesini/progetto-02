#ifndef __POTENTIOMETER__
#define __POTENTIOMETER__

class Potentiometer {
 
public:
  virtual int getValue();

protected:
  int outPin;
  int maxRead;
};

#endif
