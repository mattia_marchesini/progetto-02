#include "Led.h"
#include <arduino.h>

Led::Led(int pin) {
  this->pin = pin;
  pinMode(pin, OUTPUT);
}

void Led::switchOn() {
  digitalWrite(this->pin, HIGH);
  this->on = true;
}

void Led::switchOff() {
  digitalWrite(this->pin, LOW);
  this->on = false;
}
