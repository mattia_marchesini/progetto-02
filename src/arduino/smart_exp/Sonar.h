#ifndef __SONAR__
#define __SONAR__

#include "ProximitySensor.h"

#define STANDARD_TEMP 20

class Sonar: public ProximitySensor {

  public:
    Sonar(int echoPin, int trigPin, float temperature);
    Sonar(int echoPin, int trigPin);
    float getDistance();
    void setTemperature(int temperature);

  private:
    int echoPin, trigPin;
    float soundSpeed;
};

#endif
