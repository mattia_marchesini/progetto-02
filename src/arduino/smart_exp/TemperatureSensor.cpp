#include "TemperatureSensor.h"
#include <arduino.h>

TemperatureSensor::TemperatureSensor(int pin) {
  this->pin = pin;
}

float TemperatureSensor::getTemperature() {
  float t = analogRead(pin);
  return t * ANALOG_TO_CELSIUS_COEFF;
}
