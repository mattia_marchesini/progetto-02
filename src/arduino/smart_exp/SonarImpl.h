#ifndef __SONARIMPL__
#define __SONARIMPL__

#include "Sonar.h"

class SonarImpl: public Sonar {

  public:
    SonarImpl(int echoPin, int trigPin, int vccPin, float temperature);
    SonarImpl(int echoPin, int trigPin, int vccPin);

  private:
    int vccPin;
    void init(int vccPin);
};

#endif
