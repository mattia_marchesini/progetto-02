#include "Sonar.h"
#include <arduino.h>

float calculateSoundSpeed(float temperature) {
  float vs = 331.45 + 0.62 * temperature;
  return vs;
}

Sonar::Sonar(int echoPin, int trigPin, float temperature) {
  this->echoPin = echoPin;
  this->trigPin = trigPin;
  pinMode(echoPin, OUTPUT);
  pinMode(trigPin, OUTPUT);
  this->soundSpeed = calculateSoundSpeed(temperature);
}

Sonar::Sonar(int echoPin, int trigPin): Sonar(echoPin, trigPin, STANDARD_TEMP) { }

float Sonar::getDistance() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(3);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin, LOW);

  float tUS = pulseIn(echoPin, HIGH);
  float t = tUS / 1000.0 / 1000.0 / 2;
  float d = t * soundSpeed;
  return d;
}

void Sonar::setTemperature(int temperature) {
  this->soundSpeed = calculateSoundSpeed(temperature);
}
