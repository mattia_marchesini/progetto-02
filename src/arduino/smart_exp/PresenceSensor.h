#ifndef __PRESENCESENSOR__
#define __PRESENCESENSOR__

class PresenceSensor {
 
public: 
  virtual bool isDetected() = 0;
};

#endif
