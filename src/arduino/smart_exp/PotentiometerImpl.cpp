#include <arduino.h>
#include "PotentiometerImpl.h"

PotentiometerImpl::PotentiometerImpl(int outPin, int vccPin, int maxRead, int minMappedValue, int maxMappedValue) {
  this->outPin = outPin;
  this->vccPin = vccPin;
  this->maxRead = maxRead;
  this->minMappedValue = minMappedValue;
  this->maxMappedValue = maxMappedValue;
  pinMode(vccPin, OUTPUT);
  digitalWrite(vccPin, HIGH);
}

int PotentiometerImpl::getValue() {
  int val = analogRead(outPin);
  return map(val, 0, maxRead, minMappedValue, maxMappedValue);
}
